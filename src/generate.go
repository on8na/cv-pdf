package main

import (
	_ "embed"
	"errors"
	"io/ioutil"
	"log"
	"os"
	"strings"
	"text/template"
	"time"

	yaml "gopkg.in/yaml.v3"
)

//go:embed header.template.html
var headerText string

//go:embed footer.template.html
var footerText string

func main() {
	if len(os.Args) == 1 {
		log.Fatalf("no name given")
	}
	name := os.Args[1]

	bytes, err := ioutil.ReadFile(name + ".template.html")
	if err != nil {
		panic(err)
	}

	templateText := headerText + string(bytes) + footerText

	yamlFile, err := ioutil.ReadFile("../" + name + ".yaml")
	if err != nil {
		log.Printf("yamlFile.Get err   #%v ", err)
	}
	data := templateData{}
	err = yaml.Unmarshal(yamlFile, &data)
	if err != nil {
		log.Fatalf("Unmarshal: %v", err)
	}
	data.Timestamp = time.Now()

	for i := range data.Contact {
		c := &data.Contact[i]
		if c.Link == "" {
			c.Link = c.LinkPrefix + nospaces(c.Text)
		}
	}

	f, err := os.Create(name + ".html")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	templ, err := template.New("").Funcs(template.FuncMap{
		"strjoin": strjoin,
		"dict":    dict,
		"oneof":   oneof,
	}).Parse(templateText)
	if err != nil {
		panic(err)
	}

	err = templ.Execute(f, data)
	if err != nil {
		panic(err)
	}
}

type templateData struct {
	Timestamp time.Time
	Biography Paragrah
	Sections  map[string]templateSection
	Contact   []contactEntry
}

type templateSection struct {
	Small     bool
	Attribute string
	Items     []templateEntry
}

type templateEntry struct {
	Title    string
	Subtitle string
	Text     string
	Keywords []string
	Link     string
	Mail     string
}

type contactEntry struct {
	Icon       string
	Link       string
	Text       string
	LinkPrefix string `yaml:"link_prefix"`
}

type Paragrah string
type Attribute string

func (p *Paragrah) UnmarshalYAML(unmarshal func(interface{}) error) error {
	var nativeData string
	err := unmarshal(&nativeData)
	if err != nil {
		return err
	}

	*p = Paragrah(strings.ReplaceAll(nativeData, "\n", "<br>\n"))
	return nil
}

func oneof(inputs ...string) string {
	for _, input := range inputs {
		if input != "" {
			return input
		}
	}
	return ""
}

func strjoin(input []string, sep string) string {
	return strings.Join(input, sep)
}

// https://stackoverflow.com/a/18276968
func dict(values ...interface{}) (map[string]interface{}, error) {
	if len(values)%2 != 0 {
		return nil, errors.New("invalid dict call")
	}
	dict := make(map[string]interface{}, len(values)/2)
	for i := 0; i < len(values); i += 2 {
		key, ok := values[i].(string)
		if !ok {
			return nil, errors.New("dict keys must be strings")
		}
		dict[key] = values[i+1]
	}
	return dict, nil
}

func nospaces(input string) string {
	return strings.ReplaceAll(input, " ", "")
}
