const glob = require('glob');
const path = require('path');

const CopyWebpackPlugin = require('copy-webpack-plugin');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const {DateTime} = require('luxon');

const generateHTMLPlugins = () => glob.sync('./src/**/!(*.template).html').map(
  dir => new HTMLWebpackPlugin({
    filename: path.basename(dir),
    hash: require('child_process').execSync('git rev-parse --short HEAD').toString(),
    date: DateTime.local().toLocaleString(DateTime.DATE_MED),
    title: 'glenn',
    template: dir,
  }),
);

module.exports = {
  resolve: {
    fallback: {
      fs: false
    }
  },
  entry: ['./src/js/main.js', './src/scss/main.scss', './src/scss/a4.scss'],
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: 'index.bundle.js',
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
      },
      {
        test: /\.(pdf|gif|png|jpe?g|JPE?G|svg)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              outputPath: 'static/',
            },
          },
        ],
      },
      {
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        use: [{
          loader: 'file-loader',
          options: {
            name: '[name].[ext]',
            outputPath: 'fonts/',
          },
        }],
      },
    ],
  },
  plugins: [
    new CopyWebpackPlugin({
      patterns: [{
        from: './src/static/',
        to: './static/',
      }]
    }),
    ...generateHTMLPlugins(),
  ],
  stats: {
    colors: true,
  },
  devtool: 'source-map',
};
