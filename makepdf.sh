#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
IFS=$'\n\t\v'
cd `dirname "${BASH_SOURCE[0]:-$0}"`

NAME=${1:-}
if [[ -z "${NAME}" ]]; then
  >&2 echo "no name supplied"
  exit 1
fi

cd build
python3 -m http.server &
HTTP_PID=$!
echo "python http server PID is $HTTP_PID"
cd ..

CHROME_BIN=${2:-}
if [[ -n "${CHROME_BIN:-}" && -z "$(command -v $CHROME_BIN)" ]]; then
  >&2 echo "supplied google chrome binary not found ('$CHROME_BIN')"
  kill $HTTP_PID
  exit 1
fi
if [[ -z "${CHROME_BIN:-}" ]]; then
  for cmd in google-chrome-stable chromium-browser; do
    if command -v $cmd > /dev/null ; then
      CHROME_BIN=$(command -v $cmd)
      echo "auto-detected chrome binary: $CHROME_BIN"
      break
    fi
  done
fi
if [[ -z "${CHROME_BIN:-}" ]]; then
  >&2 echo "google chrome binary not found"
  kill $HTTP_PID
  exit 1
fi

sleep 1s
! ${CHROME_BIN} --headless --no-sandbox --disable-gpu --disable-software-rasterizer --disable-dev-shm-usage \
                --run-all-compositor-stages-before-draw --virtual-time-budget=99999999 \
                --print-to-pdf-no-header --print-to-pdf=${NAME}-big.pdf --hide-scrollbars http://localhost:8000/${NAME}.html
result=${PIPESTATUS[0]}
sleep 1s
kill $HTTP_PID
if [[ $result -ne 0 ]]; then
  >&2 echo "PDF generation failed"
  exit 1
fi

! gs -q -dNOPAUSE -dBATCH -dSAFER \
  -sDEVICE=pdfwrite \
  -dCompatibilityLevel=1.4 \
  -dPDFSETTINGS=/printer \
  -dEmbedAllFonts=true \
  -dSubsetFonts=true \
  -dAutoRotatePages=/None \
  -dColorImageDownsampleType=/Bicubic \
  -dColorImageResolution=300 \
  -dGrayImageDownsampleType=/Bicubic \
  -dGrayImageResolution=300 \
  -dMonoImageDownsampleType=/Subsample \
  -dMonoImageResolution=300 \
  -sOutputFile=${NAME}.pdf \
  ${NAME}-big.pdf

result=${PIPESTATUS[0]}
rm -f ${NAME}-big.pdf
if [[ $result -ne 0 ]]; then
  >&2 echo "PDF compression failed"
  exit 1
fi
