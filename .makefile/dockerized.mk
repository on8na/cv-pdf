##
### Docker commands
##

BUILDER_IMAGE=registry.gitlab.com/juravenator/cv-pdf/builder

docker.images: ## build docker images used for local running
	docker pull ${BUILDER_IMAGE} || echo "docker pull failed, no cache for you"
	cat .gitlab/ci/builder.Dockerfile | docker build -t ${BUILDER_IMAGE} -

docker.bash: ## open a shell in a runner container
	docker-compose -f .makefile/docker-compose.yml run --rm runner bash

docker.%: ## run any make target in a docker container
	docker-compose -f .makefile/docker-compose.yml run runner "make $(subst docker.,,$@)"